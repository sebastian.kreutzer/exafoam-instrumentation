# Instrumentation

This page serves as a tutorial on how to apply the instrumentation tools developed in T5.2 of WP5.

## Introduction
Code instrumentation is the primary method for accurate fine-grained performance measurements.
This method requires the insertion of profiling probes into the relevant functions, which in turn interface with a measurement library to record an execution profile or trace.
These profiling calls may incur a substantial runtime overhead, if every function is instrumented.
Hence, the selection of a suitable instrumentation configuration (IC), i.e. a set of instrumented functions, is vital in order to ensure that the original application behavior is retained.
Different ICs can be created based on the aspects to be measured, as well as the required level of detail.

The traditional approach is to create ICs manually in the form of filter lists.
As an alternative, the [InstRO](https://github.com/InstRO) approach performs a static analysis of the program and enables the user to set up a composable selection pipeline, which selects functions based on calling context and code metrics.  
In the context exaFOAM, we have developed a new tool called [CaPI](https://github.com/tudasc/capi.git), which is based on the same principle but is geared towards applications such as OpenFOAM.


## CaPI
CaPI addresses a few issues of the original InstRO implementation, which previously made the application on large-scale code bases difficult.
Some of the core features are:

 * A light-weight domain-specific language (DSL) to specify selection pipelines
 * Score-P integration and compatible filtering files
 * Interface for [TALP](https://pm.bsc.es/dlb) region instrumentation
 * Dynamic instrumentation with LLVM XRay 

Details can be found in our paper [1].

The general worfklow is as follows:
1. A whole-program call graph (CG) is built with [MetaCG](https://github.com/tudasc/metacg.git). 
2. The user passes a selection specification to the CaPI executable, which performs the selection on the provided CG.
3. The resulting IC file is used to perform the automatic code instrumentation. For this step, we provide a custom LLVM instrumentation pass. Alternatively, the compatible Score-P instrumenter can be used.

The produced solver binary can subsequently be executed with a compatible profiling or tracing tool.

![CaPI workflow](analysis-selection.png)

### Selection DSL
The CaPI DSL allows the user to build a pipeline of customized selector modules.

The specification consists of a sequence of selector definitions, which can be named or anonymous. 
Each definition takes a list of parameters. 
Available parameter types are strings (enclosed in double quotes), booleans (true/false), integers and floating point numbers. 
In addition, most selectors expect another selector definition as input. 
These can be either in-place definitions or references to other named selectors, marked with `%`. The selector `%%` is pre-defined and refers to an instance of the `EverythingSelector`, which selects every function in the CG.

The last definition in the sequence is used as the entry point for the selection pipeline.

For example, the following selector, named `mpi`, finds all functions starting with `MPI_`.
```
mpi = byName("MPI_.*", %%)
```

This can be used to find all functions that are on a callpath to a MPI call:

```
mpi          = byName("MPI_.*", %%)
mpi_callpath = onCallPathTo(%mpi)
```

To reduce overhead, it is typically sensible to exclude functions that are marked as `inline`.
Adding this to the previous spec, the result might look like this:

```
mpi          = byName("MPI_.*", %%)
mpi_callpath = onCallPathTo(%mpi)
final        = subtract(%mpi_callpath, inlineSpecified(%%))
```
or shortened:
```
subtract(onCallPathTo(byName("MPI_.*", %%)),inlineSpecified(%%))
```

Recently, support for loading pre-defined selection modules via the `!import` statement was added.
This allows to build and re-use selectors that are useful across multiple applications.
For example, the `mpi_callpath` selector from the previous example could be moved to a separate file:
```
!include "mpi.capi"
subtract(%mpi_callpath, inlineSpecified(%%))
```

The full DSL syntax as well as the current list of available modules is documented in the [CaPI repository](https://github.com/tudasc/capi.git).

### Instrumentation

Originally, CaPI supported only static instrumentation. 
With this approach, the code instrumentation is performed during the build by the compiler.
As a consequence, the IC is fixed for the respective build instance and cannot be changed without a rebuild.

Recently, we have added support for dynamic instrumentation based on the [LLVM XRay](https://llvm.org/docs/XRay.html) project. 
In this mode, empty instrumentation sleds are added into the binary. 
At runtime, these sleds can be rewritten in selected functions in order to call the profiling library.

The upstream version of XRay does currently not allow the use of shared library.
Since OpenFOAM implements most of its functionality in shared libraries, this version can not be used.
We provide a fork that adds this feature: [https://github.com/sebastiankreutzer/llvm-project-xray-dso](https://github.com/sebastiankreutzer/llvm-project-xray-dso).
We are working on making this available in the standard Clang release.


## Application on OpenFOAM

We maintain a repository containing scripts and configuration files for the application of CaPI on OpenFOAM: [exafoam-instrumentation](https://git.rwth-aachen.de/sebastian.kreutzer/exafoam-instrumentation.git).

## MetaCG Call Graph

Scripts for the creation of a MetaCG file for OpenFOAM are available in the repository. However, the required set up can be quite tedious.
For this reason, we provide pre-built CG files for certain solvers in the `callgraphs` directory.

## Generating a new IC with CaPI

In order to use CaPI, please perform the necessary installation steps described in the [CaPI repository](https://github.com/tudasc/capi.git).

CaPI can then be invoked as follows: `capi -i <selection_spec> <cgfile>`.
You can either choose to use one of the example specs in the instrumentation repository or define your own.


## Instrumented OpenFOAM build

We currently advise using the dynamic instrumentation feature of CaPI, rather than working with the static approach, as this saves a lot of time and disk space.

In order to use it, an XRay-instrumented build of OpenFOAM is needed.
The required `wmake` configuration files can be found in the [exafoam-instrumentation](https://git.rwth-aachen.de/sebastian.kreutzer/exafoam-instrumentation.git) repository. 
For instructions on how to install and build, refer to the README in this repository.

## Running OpenFOAM
The default instrumentation interface we use is compatible with the GNU `-finstrument-functions` option.
In principle, any tool supporting that interface can be used.

However, we are currently focusing on providing support for [ScoreP](https://www.vi-hps.org/projects/score-p/) and [TALP](https://pm.bsc.es/dlb). 

### ScoreP

TODO

### TALP

TODO

# Notes
This pages is not complete at the moment. Contact Sebastian Kreutzer (sebastian.kreutzer@tu-darmstadt.de) if you have any questions.


# References
[1] Sebastian Kreutzer, Christian Iwainsky, Jan-Patrick Lehr, Christian Bischof, "Compiler-assisted Instrumentation 
Selection for Large-scale C++ Codes.", C3PO’22 Workshop - Third Workshop on Compiler-Assisted Correctness 
Checking and Performance Optimization for HPC, Jun 2022, Hamburg, Germany (to appear)

