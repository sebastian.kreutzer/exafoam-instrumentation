
import sys
import numpy as np
import matplotlib.pyplot as plt

from numpy import log2

def eval_model(model_str, param_name, val):
  exec("%s = %f" % (param_name, val))
  return eval(model_str)

if __name__ == "__main__":
  param_name = sys.argv[1]
  infile = sys.argv[2]
  maxval = sys.argv[3]

  with open(infile) as f:
    lines = f.readlines()

  param_vals = [int(i) for i in lines[0].split()]


  plt.title("Performance models")
  plt.xlabel("Time")
  plt.ylabel(param_name)

  for i in range(1, len(lines), 3):
      fname = lines[i]
      model_str = lines[i+1]
      model_str = model_str[7:]
      measured_vals = [float(j) for j in lines[i+2].split()]

      plt.plot(param_vals, measured_vals)

      print(fname, model_str)
      print(eval_model(model_str, param_name, 64))


  plt.show()



