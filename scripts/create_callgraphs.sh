#!/bin/bash

if [ ! -d $WM_PROJECT_DIR ]; then
  echo  "OpenFOAM not found."
  exit
fi

find "$WM_PROJECT_DIR" -name "*.ipcg" -delete

cgc_exe="${CGC_DIR:-$HOME/git/MetaCG/cgcollector/build}/tools/cgcollector"

if [ ! -x $(command -v $cgc_exe) ]; then
  echo  "CGCollector not found in ${cgc_exe}.\nPlease set CGC_DIR to your CGCollector build directory."
  exit
fi


clang_headers="${CLANG_DIR:-/opt/modules/packages/llvm/11.1.0/lib/clang/11.1.0}/include"

if [ ! -d $clang_headers ]; then
  echo "Clang include directory not found in ${clang_headers}.\nMake sure to set CLANG_DIR correctly."
  exit
fi

src_dir="$WM_PROJECT_DIR/src"
app_dir="$WM_PROJECT_DIR/applications"


for f in $(find $src_dir -type f \( -iname  "*.c" -o -iname "*.cpp" \) ); do

  echo "Building callgraph for $f"
  "$cgc_exe" --capture-ctors-dtors --metacg-format-version=2 $f --extra-arg="-I${clang_headers}"
done

for f in $(find $app_dir -type f \( -iname  "*.c" -o -iname "*.cpp" \) ); do

  echo "Building callgraph for $f"
  "$cgc_exe" --capture-ctors-dtors --metacg-format-version=2 $f --extra-arg="-I${clang_headers}"
done
