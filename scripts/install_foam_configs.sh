#!/bin/bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"


test_xray_dso() {
     echo "void foo() {}" | clang++ -c -fxray-enable-shared -o /dev/null -x c++ - &> /dev/null
}


if [ ! -d "$WM_PROJECT_DIR" ]; then
    echo "\$WM_PROJECT_DIR is not set. Please source your OpenFOAM installation or set \$WM_PROJECT_DIR manually."
    exit
fi

echo "Copying wmake rules..."
cp -r "$script_dir"/../openfoam_config/wmake/* "$WM_PROJECT_DIR/wmake/"

echo "Installing Score-P configuration..."
scorep_etc_dir="$WM_PROJECT_DIR/etc-scorep"
if [ -d "$scorep_etc_dir" ]; then
    echo "Directory $scorep_etc_dir already exists."
    echo "Remove it and run this script again to re-generate."
else
    cp -r "${script_dir}/../openfoam_config/etc-scorep" "$WM_PROJECT_DIR"
fi

echo "Installing CaPI configurations..."
if [ ! -d "$CAPI_LIB_DIR" ]; then
    capi_exe=$(command -v capi)
    if [ -x "$capi_exe" ]; then
        CAPI_LIB_DIR=$(realpath "$(dirname $capi_exe)/../lib")
    else
        echo "CaPI  executable not found."
        echo "Please adjust your PATH or directly set \$CAPI_LIB_DIR to the library directory of your CaPI installation."
        exit
    fi
fi

if [ ! test_xray_dso ]; then
    echo "Your Clang/LLVM installation does not support the instrumentation of shared libraries. This will limit the scope of the OpenFOAM measurements significantly."
    echo "Consider installing this Clang fork: git@github.com:sebastiankreutzer/llvm-project-xray-dso.git"
fi


#if [ ! -x "$(command -v llvm-config)" ]; then
#    echo "The llvm-config binary could not be found. Please make sure the LLVM binary directory is in your PATH, then retry."
#    exit
#fi

#llvm_libs=$(llvm-config --libfiles xray symbolize --link-static --system-libs)

#if [ -x "$(command -v extrae_static)" ]; then
    dyncapi_extrae_etc_dir="$WM_PROJECT_DIR/etc-dyncapi-extrae"

    if [ -d "$dyncapi_extrae_etc_dir" ]; then
        echo "Directory $dyncapi_extrae_etc_dir already exists. Remove it and run this script again to re-generate."
    else
        mkdir "$dyncapi_extrae_etc_dir"

        cat > $dyncapi_extrae_etc_dir/prefs.sh << EOF
etc_dir=\$(realpath "\$(dirname "\${BASH_SOURCE[0]}")")

export FOAM_CONFIG_ETC="etc-dyncapi-extrae"

export WM_COMPILER=Clang
export WM_COMPILE_OPTION=Extrae

export CAPI_LIB_DIR="${CAPI_LIB_DIR}"

EOF

    echo "CaPI-Extrae config successfully installed. Load it with the following command:"
    echo "  source $WM_PROJECT_DIR/etc/bashrc $dyncapi_extrae_etc_dir/prefs.sh"
    echo ""

    fi

#else
#    echo "Could not detect Extrae. Please install Extrae and retry."
#fi


if [ -x "$(command -v scorep)" ]; then

    dyncapi_scorep_etc_dir="$WM_PROJECT_DIR/etc-dyncapi-scorep"

    if [ -d "$dyncapi_scorep_etc_dir" ]; then
        echo "Directory $dyncapi_scorep_etc_dir already exists. Remove it and run this script again to re-generate."
    else
        mkdir "$dyncapi_scorep_etc_dir"

        scorep-config --mpp=mpi --adapter-init > "$dyncapi_scorep_etc_dir/scorep_init.c"
        clang -c "$dyncapi_scorep_etc_dir/scorep_init.c" -o "$dyncapi_scorep_etc_dir/scorep_init.o"

        cat > $dyncapi_scorep_etc_dir/prefs.sh << EOF
etc_dir=\$(realpath "\$(dirname "\${BASH_SOURCE[0]}")")

export FOAM_CONFIG_ETC="etc-dyncapi-scorep"

export WM_COMPILER=Clang
export WM_COMPILE_OPTION=DynScoreP

export SCOREP_INIT="\${etc_dir}/scorep_init.o"
export CAPI_LIB_DIR="${CAPI_LIB_DIR}"

EOF

    echo "CaPI-ScoreP config successfully installed. Load it with the following command:"
    echo "  source $WM_PROJECT_DIR/etc/bashrc $dyncapi_scorep_etc_dir/prefs.sh"

    fi

else
    echo "The scorep command is not available. Please install Score-P and retry."
fi



