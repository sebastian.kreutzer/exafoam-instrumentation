#!/bin/bash

if [ ! -d "$WM_PROJECT_DIR" ]; then
  echo  "OpenFOAM not found."
  exit
fi


cg_list_file=${1:-cg_list_${WM_PROJECT_VERSION}.txt}


cgm_exe="${CGC_DIR:-/work/projects/project01667/MetaCG/build/cgcollector/tools}/cgmerge"

if [ ! -x $(command -v $cgm_exe) ]; then
  echo  "CGMerge not found in ${cgm_exe}.\nPlease set CGC_DIR to your CGCollector build directory."
  exit
fi


clang_headers="${CLANG_DIR:-/opt/modules/packages/llvm/11.1.0/lib/clang/11.1.0}/include"

if [ ! -d $clang_headers ]; then
  echo "Clang include directory not found in ${clang_headers}.\nMake sure to set CLANG_DIR correctly."
  exit
fi

src_dir="$WM_PROJECT_DIR/src"


IPCG_FILENAME="openfoam_${WM_PROJECT_VERSION}.ipcg"
echo "null" > $IPCG_FILENAME
echo "Executing cgmerge: $cgm_exe $IPCG_FILENAME  \$(cat $cg_list_file)"
$cgm_exe $IPCG_FILENAME  $(cat $cg_list_file)


