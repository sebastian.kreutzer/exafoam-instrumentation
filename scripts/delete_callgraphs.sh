#!/bin/bash

if [ ! -d $WM_PROJECT_DIR ]; then
  echo  "OpenFOAM not found."
  exit
fi

find "$WM_PROJECT_DIR" -name "*.ipcg" -delete

