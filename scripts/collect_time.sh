#!/bin/bash

module load python


dir=${1:-"."}
function=$2

profiles=$(find "$dir" -name "*.cubex")

echo "Found $(echo $profiles | wc -w) profiles"


echo "Collecting inclusive execution time for $2"
echo "*********************************************"

if [ "$profiles" = "" ]; then
	echo "No profiles found."
	exit
fi

for prof in $profiles; do
	echo "Time in $2 for ${prof}:"
	cube_info -t "$prof" | grep -E "$2"
done


