#!/bin/bash

if [ ! -d "$WM_PROJECT_DIR" ]; then
  echo  "OpenFOAM not found."
  exit
fi

num_procs=${1:-"1"}

cg_list_file=${2:-cg_list_${WM_PROJECT_VERSION}.txt}



cgm_exe="${CGC_DIR:-/work/projects/project01667/MetaCG/build/cgcollector/tools}/cgmerge"

if [ ! -x $(command -v $cgm_exe) ]; then
  echo  "CGMerge not found in ${cgm_exe}.\nPlease set CGC_DIR to your CGCollector build directory."
  exit
fi


clang_headers="${CLANG_DIR:-/opt/modules/packages/llvm/11.1.0/lib/clang/11.1.0}/include"

if [ ! -d $clang_headers ]; then
  echo "Clang include directory not found in ${clang_headers}.\nMake sure to set CLANG_DIR correctly."
  exit
fi

function merge_files {
  merge_target_file="$1"
  in_files=( "${@:2}" )
  echo "null" > "$merge_target_file"
  echo "Executing cgmerge: $cgm_exe $merge_target_file: ${#in_files[@]} input files"
  $cgm_exe $merge_target_file ${in_files[@]}
}

src_dir="$WM_PROJECT_DIR/src"

cg_files=($(cat $cg_list_file))

partial_files=()

count=${#cg_files[@]}
np=$num_procs
batch_size=$(((count+np-1)/np))

echo "Merging with $num_procs processors, $batch_size files per process"


for ((i = 0; i < ${#cg_files[@]}; i += batch_size)); do
  batch=${cg_files[@]:i:batch_size}
  part_file="openfoam_${WM_PROJECT_VERSION}.ipcg.part.${#partial_files[@]}"
  partial_files+=("$part_file")
  merge_files "$part_file" "${batch[@]}" &
done

wait

echo "Executing final merge!"
IPCG_FILENAME="openfoam_${WM_PROJECT_VERSION}.ipcg"
merge_files "$IPCG_FILENAME" "${partial_files[@]}"

echo "Done!"




