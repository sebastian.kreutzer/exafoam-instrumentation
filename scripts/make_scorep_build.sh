#!/bin/bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

solver="$1"
filter_file="$2"
name="$3"

filter_filename=$(basename "$filter_file")

foam_dir=${WM_PROJECT_DIR}

etc_dir="${foam_dir}/etc-scorep-${name}"

if [ ! -d "$foam_dir" ]; then
  echo "WM_PROJECT_DIR not set. Please load OpenFOAM and try again."
  exit
fi

mkdir $etc_dir

cp $filter_file $etc_dir


prefs_file="$etc_dir/prefs.sh"
cat > $prefs_file << EOL

export FOAM_CONFIG_ETC="$etc_dir"

export SCOREP_WRAPPER_COMPILER_FLAGS=
export SCOREP_WRAPPER_INSTRUMENTER_FLAGS="--mpp=mpi --instrument-filter=${etc_dir}/${filter_filename}"
export SCOREP_PROFILING_MAX_CALLPATH_DEPTH=64

# Score-P can either support generating serial code or parallel/mpi code (default=parallel) - but never both at the same time.
# If you run an application instrumented by Score-P with parallel support in serial,
# Score-P will not be able to dump its results to disk but will report an error at application end.
# For an OpenFOAM installation for serial support uncomment the following.
#export SCOREP_WRAPPER_INSTRUMENTER_FLAGS="--mpp=none ${SCOREP_WRAPPER_INSTRUMENTER_FLAGS}"

# configure environment
export WM_COMPILER=GccScoreP_${name}

EOL

# Create "fake" compiler for this build
cp -r $foam_dir/wmake/rules/linux64GccScorePWrapper/ $foam_dir/wmake/rules/linux64GccScoreP_${name}

echo "Done. Build OpenFOAM now? y/n"

read build

if [ "$build" = "y" ]; then
  module purge;
  module load gcc/10.2;
  module load openmpi/4.1;
  module load boost/1.75.0;
  module load cmake;
  module load fftw/3.3.9;
  module load cgal/5.2.0;
  module load scotch;
  module load papi;
  module load scorep/7.1;

  source $foam_dir/etc/bashrc $prefs_file
  echo "Building SCOREP config with filter file ${filter_file}"
  cd $foam_dir
  ./Allwmake -j -l
fi

