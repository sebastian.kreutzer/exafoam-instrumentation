#!/bin/bash

if [ ! -d $WM_PROJECT_DIR ]; then
  echo  "OpenFOAM not found."
  exit
fi


src_dir="$WM_PROJECT_DIR/src"
outfile=${1:-cg_list_${WM_PROJECT_VERSION}.txt}

cg_files=$(find $src_dir -name "*.ipcg")
echo "Found files: $cg_files"
rm "$outfile"
for cg in $cg_files; do
  echo "Checking CG file: $cg"
  if [ "$(head -n 1 $cg)" != "null" ]; then
    echo "$cg is valid"
    echo $cg >> "$outfile"
  fi
done
echo "Done."


