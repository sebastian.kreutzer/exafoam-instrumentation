#!/bin/bash

if [ ! -d $WM_PROJECT_DIR ]; then
  echo  "OpenFOAM not found."
  exit
fi

CGC_DBG="$CGC_DIR/../build-debug"
cgm_exe="${CGC_DBG:-$HOME/git/MetaCG/cgcollector/build}/tools/cgmerge"


if [ ! -x $(command -v $cgm_exe) ]; then
  echo  "CGMerge not found in ${cgm_exe}.\nPlease set CGC_DIR to your CGCollector build directory."
  exit
fi


clang_headers="${CLANG_DIR:-/opt/modules/packages/llvm/11.1.0/lib/clang/11.1.0}/include"

if [ ! -d $clang_headers ]; then
  echo "Clang include directory not found in ${clang_headers}.\nMake sure to set CLANG_DIR correctly."
  exit
fi

src_dir="$WM_PROJECT_DIR/src"


IPCG_FILENAME=openfoam.ipcg
echo "null" > $IPCG_FILENAME
#find $src_dir -name "*.ipcg" -exec $cgm_exe $IPCG_FILENAME {} +
find $src_dir -name "*.ipcg" > cg_list.txt

echo "gdb --args $cgm_exe $IPCG_FILENAME  $(cat cg_list_test.txt)"


