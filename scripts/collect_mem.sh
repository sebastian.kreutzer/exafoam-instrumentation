#!/bin/bash

basedir=$1

for f in $(find "$basedir" -name "solve_*.err"); do
  np=$(cat $f | grep -c "Elapsed time:")
  echo "$f (np=$np)"
  echo $(cat $f | grep Memory | tail -n "$np" | awk '{print $2}' | paste -sd+ | bc)/1000 | bc
done

