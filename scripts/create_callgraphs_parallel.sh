#!/bin/bash

if [ ! -d "$WM_PROJECT_DIR" ]; then
  echo  "OpenFOAM not found."
  exit
fi


cgc_exe="${CGC_DIR:-/work/projects/project01667/MetaCG/build/cgcollector/tools}/cgcollector"

if [ ! -x $(command -v $cgc_exe) ]; then
  echo  "CGCollector not found in ${cgc_exe}.\nPlease set CGC_DIR to your CGCollector build directory."
  exit
fi


clang_headers="${CLANG_DIR:-/opt/modules/packages/llvm/11.1.0/lib/clang/11.1.0}/include"

if [ ! -d $clang_headers ]; then
  echo "Clang include directory not found in ${clang_headers}.\nMake sure to set CLANG_DIR correctly."
  exit
fi

src_dir="$WM_PROJECT_DIR/src"
app_dir="$WM_PROJECT_DIR/applications"

source_list=($(find $src_dir -type f \( -iname  "*.c" -o -iname "*.cpp" \) ))
app_list=($(find $app_dir -type f \( -iname  "*.c" -o -iname "*.cpp" \) ))

source_list+=( "${app_list[@]}" )

echo "Source files found: ${#source_list[@]}"

num_procs=${1:-32}
cc_file=${2:-$(find $WM_PROJECT_DIR -name "compile_commands.json" | head -n 1)}

echo "Running with $num_procs processes, using compilation database: $cc_file"

for ((idx=0; idx < ${#source_list[@]}; idx+=$num_procs)); do
    # act on ${a[$idx]}

    for ((j=0; j + idx < ${#source_list[@]} && j < $num_procs; ++j)); do
        k=$(( $j + $idx ))
        echo "Command: $cgc_exe" -p "$cc_file" --capture-ctors-dtors --metacg-format-version=2 "${source_list[$k]}" --extra-arg="-I${clang_headers}"
        "$cgc_exe" -p "$cc_file" --capture-ctors-dtors --metacg-format-version=2 "${source_list[$k]}" --extra-arg="-isystem${clang_headers}" &
        #echo "$idx, $j: ${source_list[$k]}" &
    done
    wait

    echo "Batch done: $idx"
done
