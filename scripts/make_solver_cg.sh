#!/bin/bash

if [ ! -d $WM_PROJECT_DIR ]; then
  echo  "OpenFOAM not found."
  exit
fi


cgm_exe="${CGC_DIR:-/work/projects/project01667/MetaCG/build/cgcollector/tools}/cgmerge"

if [ ! -x $(command -v $cgm_exe) ]; then
  echo  "CGMerge not found in ${cgm_exe}.\nPlease set CGC_DIR to your CGCollector build directory."
  exit
fi


clang_headers="${CLANG_DIR:-/opt/modules/packages/llvm/11.1.0/lib/clang/11.1.0}/include"

if [ ! -d $clang_headers ]; then
  echo "Clang include directory not found in ${clang_headers}.\nMake sure to set CLANG_DIR correctly."
  exit
fi

src_dir="$WM_PROJECT_DIR/src"

main_cg=${1:-openfoam.ipcg}

if [ ! -f $main_cg ]; then
  echo "Invalid main CG file. Please pass a valid CG file as the first argument."
  exit
fi

solver_name=$2
if [ -z $solver_name ]; then
  echo "Please pass the name of the solver as the second argument."
  exit
fi

solver_dir=$(find $WM_PROJECT_DIR/applications/ -name $solver_name)

echo "Generating merged CG for $solver_name"

solver_cg="${solver_dir}/${solver_name}.ipcg"

if [ ! -f $solver_cg ]; then
  echo "CG file does not exist: $solver_cg"
fi

outfile="openfoam_${solver_name}.ipcg"
echo "null" > $outfile

$cgm_exe $outfile $main_cg $solver_cg

echo "Done."
