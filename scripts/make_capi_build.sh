#!/bin/bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

solver="$1"
filter_file="$2"
name="$3"
api="$4"


if [ "$api" != "gnu" ] && [ "$api" != "talp" ]; then
  echo "Please choose \"gnu\" or \"talp\" API"
  exit
fi

filter_filename=$(basename "$filter_file")

foam_dir=${WM_PROJECT_DIR}

etc_dir="${foam_dir}/etc-capi-${name}-${api}"

if [ ! -d "$foam_dir" ]; then
  echo "WM_PROJECT_DIR not set. Please load OpenFOAM and try again."
  exit
fi

mkdir $etc_dir

cp $filter_file $etc_dir

cp $script_dir/scorep-link $etc_dir



prefs_file="$etc_dir/prefs.sh"
cat > $prefs_file << EOL
export FOAM_CONFIG_ETC="$etc_dir"

# configure environment
export WM_COMPILER=ClangCaPI_${name}_${api}


dir=\$(realpath "\$(dirname "\${BASH_SOURCE[0]}")")
export SCOREP_LINK=\$dir/scorep-link
export CAPI_LIB_DIR=/work/projects/project01667/instro-llvm/build-release/lib
export SCOREP_INJECTOR_DIR=/work/projects/project01667/scorep-symbol-injector/build/lib

export CAPI_FILTER_FILE=${etc_dir}/${filter_filename}

EOL

# Create "fake" compiler for this build
#cp -r $foam_dir/wmake/rules/linux64ClangInstro/ $foam_dir/wmake/rules/linux64ClangCapi_${name}_${api}
base_cfg=linux64ClangCaPI
if [ "$api" = "talp" ]; then
  base_cfg=linux64ClangCaPITALP
fi
cp -r $foam_dir/wmake/rules/$base_cfg $foam_dir/wmake/rules/linux64ClangCaPI_${name}_${api}

echo "Done. Build OpenFOAM now? y/n"

read build

if [ "$build" = "y" ]; then
  module purge;
  module load gcc/10.2;
  module load llvm/10.0.0;
  module load openmpi/4.1;
  module load boost/1.75.0;
  module load cmake;
  module load fftw/3.3.9;
  module load cgal/5.2.0;
  module load scotch;
  module load papi;
  module load scorepnoplugin/7.1;
  source $foam_dir/etc/bashrc $prefs_file
  echo "Building CaPI config with filter file $CAPI_FILTER_FILE"
  cd $foam_dir
  ./Allwmake -j -l
fi

