#!/bin/bash

echo "Creating CG backup in $1"
mkdir $1
mkdir $1/src
mkdir $1/applications

find $WM_PROJECT_DIR/src -name "*.ipcg" -exec cp {} $1/src \;
find $WM_PROJECT_DIR/applications -name "*.ipcg" -exec cp {} $1/applications \;
