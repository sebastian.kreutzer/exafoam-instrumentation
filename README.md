# ExaFOAM Instrumentation

This repository contains a collection of configuration files and scripts for the instrumentation and performance evaluation of OpenFOAM.

The `openfoam_jobgen` contains a script for generating scaling jobs for selected exaFOAM benchmarks.
See the contained README file for usage instructions.
The `extrap_converter` directory contains scripts for processing the profile information gathered in the scaling experiments and creating an input file for Extra-P, for the generation of scaling models. 

We currenly provide multiple configurations for instrumenting OpenFOAM:
- A statically instrumented version of OpenFOAM using [ScoreP](https://www.vi-hps.org/projects/score-p/). 
- Multiple configurations using [CaPI](https://github.com/tudasc/capi)  with dynamic LLVM-XRay instrumentation

## Setup

The `install_foam_configs.sh` script generates the wmake configurations for your OpenFOAM installation.

Steps:
- Install the optional CaPI dependencies (see [CaPI](#CaPI))
- Source your OpenFOAM installation (or set `$WM_PROJECT_DIR` by hand)
- Run `scripts/install_foam_configs.sh`


## Using ScoreP with OpenFOAM

First, you need to install a current version of ScoreP, which can be found [here](https://www.vi-hps.org/projects/score-p/).
Make sure to build with `--enable-shared`.

To compile an instrumented version of OpenFOAM:
- Reload the `bashrc` file: `source $WM_PROJECT_DIR/etc/bashrc $WM_PROJECT_DIR/etc-scorep/prefs.sh`. This will set `$WM_COMPILER` to `GccScoreP`.
- Rebuild OpenFOAM with `Allwmake`.

Note: The instrumentation of third-party libraries is currenlty not supported.

### Profiling a run

To generate a ScoreP profile, first set the following environment variables:
```
export SCOREP_ENABLE_PROFILING=true
export SCOREP_ENABLE_TRACING=false
```
You may also want to specify recording additional hardware counters, e.g. `export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_TOT_CYC`.

When the target application is run, ScoreP will generate a profile containing performance data of each executed function. This profile can be examined with the Cube tool, which is contained in the ScoreP installation.

See the [ScoreP documentation](https://perftools.pages.jsc.fz-juelich.de/cicd/scorep/tags/scorep-7.1/pdf/scorep.pdf) for further information.


## CaPI

To run OpenFOAM with CaPI, you need the following:
1. A modified version of Clang that enables instrumentation of shared libraries (DSOs)
2. CaPI (`devel` branch for current development version)
3. OpenFOAM built with the modified Clang and linked with the CaPI runtime libs

### Installing Clang/LLVM
- Download from https://github.com/sebastiankreutzer/llvm-project-xray-dso (make sure to switch to branch `xray-dso`)
- Build:
	- This assumes you use the [Ninja](https://github.com/ninja-build/ninja) build system, you can use standard `make` instead. Simply leave out the `-G Ninja` parameter in the cmake configuration.
	-  `mkdir build && cd build`
	- `cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/path/to/install/llvm/13.0.1-xray-dso -DLLVM_ENABLE_PROJECTS="clang;compiler-rt;libcxx;libcxxabi" -DLLVM_TARGETS_TO_BUILD=X86 -DLLVM_INSTALL_UTILS=ON  ../llvm/`
	- `ninja`
	- `ninja install`
- If your system supports modules, consider adding a modulefile so that clang can easily be loaded.

Example modulefile:
```
#%Module1.0
proc ModulesHelp { } {
global dotversion

puts stderr "\tLLVM 13.0.1 with XRay DSO instrumentation"
}

module-whatis "LLVM 13.0.1 with XRay DSO instrumentation"
conflict llvm
prepend-path PATH /opt/modules/packages/llvm/13.0.1-xray-dso/bin
prepend-path LD_LIBRARY_PATH /opt/modules/packages/llvm/13.0.1-xray-dso/lib
prepend-path LIBRARY_PATH /opt/modules/packages/llvm/13.0.1-xray-dso/lib
prepend-path MANPATH /opt/modules/packages/llvm/13.0.1-xray-dso/share/man
```

### Installing CaPI
- Download CaPI from `https://github.com/tudasc/CaPI`
- Switch to branch `devel`
- Build:
	- Make sure the custom Clang is in `PATH` and set `CC` and `CXX` environment variables to clang and clang++: `export CC=$(which clang); export CXX=$(which clang++)`
	- `mkdir build && cd build`
	- `cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DSCOREP_SUPPORT=OFF -DENABLE_TALP=OFF ..`
		- Or with optional TALP support: `cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DENABLE_TALP=ON -DSCOREP_SUPPORT=OFF -DDLB_DIR=/path/to/dlb ..`
	- `ninja`

### Verifying your installation
- In your build folder, go to the `examples/lulesh` directory
- Build with `make lulesh2.0_extrae`
- Run with Extrae and instrumentation: `EXTRAE_ON=1 CAPI_ENABLE=1 mpirun -n 1 --bind-to core env LD_PRELOAD=/usr/lib/libmpitrace.so ./lulesh2.0_extrae`
	- This will instrument and record all functions


### Known Issues
There are some versions of the GNU standard C++ library implementation (stdlibc++) that seem to be incompatible with LLVM's `compiler-rt`, which is the base for the CaPI runtime libraries.
This is likely related to the following bug: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=88782
As a workaround, build CaPI and OpenFOAM with LLVM's own standard library implementation (libc++).


- First, you need to rebuild LLVM in order to use libc++, using your previous Clang build :
	- Make sure the custom Clang is in `PATH` and set `CC` and `CXX` environment variables to clang and clang++: `export CC=$(which clang); export CXX=$(which clang++)`
	- `mkdir build-libcxx && cd build-libcxx`
	- `cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/path/to/install/llvm/13.0.1-xray-dso-libcxx -DLLVM_ENABLE_PROJECTS="clang;compiler-rt;libcxx;libcxxabi" -DLLVM_TARGETS_TO_BUILD=X86 -DLLVM_INSTALL_UTILS=ON -DLLVM_ENABLE_LIBCXX=ON -DCLANG_DEFAULT_CXX_STDLIB=libc++ ../llvm/`
	- `ninja`
	- `ninja install`
	- If desired, create a new modulefile for this clang variant
- Now rebuild CaPI with this Clang build






