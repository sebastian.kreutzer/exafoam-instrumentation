# openfoam_jobgen

This script simplifies the generation of scaling experiments for OpenFOAM benchmark cases.
For a given benchmark and scaling configuration, it sets up the required directory structure, copies over the required case setup files and generates job scripts for preprocessing and executing the solver.

## Case Module
A case module needs to be provided for each new benchmark case. 
This is fairly straightforward to implement. The following functions need to be defined:
- `copy_setup(case_dir, target)`: Copy over the required benchmark files from the original `case_dir` to `target`.
- `configure(working_dir, num_nodes, num_procs)`: Configure the OpenFOAM setup for execution with the given number of nodes and processes. Typically, this involves to set `numberOfSubdomains` in `system/decomposeParDict`.
- `create_preproc_script(foam_env,  working_dir, num_nodes, num_procs)`: Generate the preprocessing job script that handles meshing and decomposition. The `JobFileWriter` class can be used to simplify this step.
- `create_solve_script(foam_env, filter_file, papi_ctrs, working_dir, num_nodes, num_procs)`: Generate the solve job script that executes and measures the solver. The `JobFileWriter` class can be used to simplify this step.

You can find an example for the lid-driven cavity benchmark in `cavity.py`.

## Usage
The script enables the user to specify:
- The output directory
- The PAPI counters to measure
- The Score-P filter file
- The number of nodes and processes to generate configurations for, given as a python expression that evaluates to a mapping from node counts to a list of process counts. For example, `{1: [10], 2: [20]}` refers to configurations with one and 2 nodes, with 10 and 20 processes respectively.

Run `python jobgen.py -h` to print detailed help information.

Example for cavity:
```python jobgen.py -i /work/projects/project01921/benchmark_repos/openfoam-hpc/Lid_driven_cavity-3d/S/ -o "$HPC_SCRATCH" --scale "{1: [64, 96], 2: [192]}" --name "cavity_scaling_test" cavity```

This will generate configurations for 64 and 96 processes on one node and 192 on two nodes. For each configuration, a directory with the configured setup will be written to $HPC_SCRATCH/cavity_scaling_test/.
