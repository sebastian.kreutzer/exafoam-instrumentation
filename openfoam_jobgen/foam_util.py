import subprocess


def set_dict_entry(dict_file, entry, val):
    try:
        subprocess.run(["foamDictionary", dict_file, "-entry", entry, "-set", str(val)], check=True)
    except subprocess.CalledProcessError:
        raise RuntimeError("Error setting dictionary entry")


