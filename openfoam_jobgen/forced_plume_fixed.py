import shutil
import os

from jobfile import JobFileWriter
import foam_util

def copy_setup(case_dir, target):
    # Simply copy everything
    #for f in os.listdir(case_dir):
    #   shutil.copytree(os.path.join(case_dir, f), os.path.join(target, f))
    shutil.copytree(case_dir, target)

def configure(working_dir, num_nodes, num_procs):
    foam_util.set_dict_entry(os.path.join(working_dir, "system/decomposeParDict"), "numberOfSubdomains", num_procs)
    foam_util.set_dict_entry(os.path.join(working_dir, "system/myDict"), "myEndTime", 0.003)



def create_preproc_script(foam_env, working_dir, num_nodes, num_procs, cpu_bind):
    job_name = f"forcedPlume_preproc_{num_procs}"
    preproc_job = JobFileWriter(job_name, foam_env.foam_dir)
    preproc_job.write_header(working_dir, 1, 1, "4000M", 10, False)
    preproc_job.load_modules(foam_env.get_modules("gcc"))
    preproc_job.load_foam_cfg(foam_env.get_prefs_file_full("gcc"))
    preproc_job.load_run_functions()

    preproc_job.write_line("runApplication blockMesh")
    preproc_job.write_line("runApplication decomposePar")

    job_filename = f"{job_name}.job"
    preproc_job.output_to_file(os.path.join(working_dir, job_filename))
    return job_filename

def create_solve_script(foam_env, filter_file, papi_ctrs, working_dir, num_nodes, num_procs, cpu_bind):
    job_name = f"forced_plume_solve_{num_procs}"
    solve_job = JobFileWriter(job_name, foam_env.foam_dir)
    solve_job.write_header(working_dir, num_nodes, num_procs, "4000M", 10, True)
    solve_job.load_modules(foam_env.get_modules("scorep"))
    solve_job.load_foam_cfg(foam_env.get_prefs_file_full("scorep"))
    solve_job.load_run_functions()
    solve_job.configure_scorep_profiling(filter_file, papi_ctrs, f"scorep-{num_procs}")

    solve_job.timed_srun("rhoPimpleFoam -parallel", "rhoPimpleFoam", cpu_bind)

    job_filename = f"{job_name}.job"
    solve_job.output_to_file(os.path.join(working_dir, job_filename))
    return job_filename
