import sys
import os
import stat
import io
import shutil
import argparse
from collections.abc import Mapping

def main():
    print("OpenFOAM Scaling Job Generator")
    print("******************************")


    #TODO: Parse options:
    # - Where the base files for the  experiment are located
    # - What the module is called
    # - Which process sizes to generate
    # - Where to put the experiment
    parser = argparse.ArgumentParser(prog='jobgen.py', description='Automatic generation of job files for OpenFOAM benchmark cases.')
    parser.add_argument('module', help='Benchmark configuration module')
    parser.add_argument('-i', help='The directory of the benchmark setup to copy', metavar='benchmark_dir', required=True)
    parser.add_argument('-o', help='Where to generate the experiment setup', metavar='target_dir')
    parser.add_argument('--papi', help='A list of PAPI counters to measure', default='PAPI_TOT_CYC,PAPI_TOT_INS', metavar='papi_ctrs')
    parser.add_argument('--scale', help='Process configurations to generate', required=True, metavar='scale_expr')
    parser.add_argument('--filter', help='The Score-P filter file to use', metavar='filter_file')
    parser.add_argument('--name', help='Name of the experiment', required=True, metavar='name')
    parser.add_argument('--cpu-bind', help='The --cpu-bind parameter passed to the srun command', default='cores', metavar='cpu_bind')
    parser.add_argument('--mesh-dir', help='Directory containing the pre-generated mesh to use', default=None, metavar='mesh_dir')

    args = parser.parse_args()


#    if args.module is None or not os.path.isfile(args.module):
#        raise ValueError(f"Could not find case module: {args.module}")
    #case_module_name = "cavity"


    case_dir = args.i
    if not os.path.isdir(case_dir):
        raise ValueError(f"Input case directory does not exists: {case_dir}")

    #case_dir = os.path.join(base_target_dir, "benchmark_repos/openfoam-hpc/Lid_driven_cavity-3d/S/")

    base_target_dir = args.o
    if not os.path.isdir(base_target_dir):
        raise ValueError(f"Output directory does not exist: {base_target_dir}")
    #base_target_dir = "/work/projects/project01921/"

    papi_ctrs = args.papi
    #papi_ctrs = "PAPI_TOT_INS,PAPI_TOT_CYC"

    proc_configs = eval(args.scale)
    if not isinstance(proc_configs, Mapping):
        raise ValueError(f"Scale configurations needs to be a dict mapping the number of nodes to a list of number of processes. Invalid input: {proc_configs}")

    print(f"Generating scaling experiments for the following configurations: {proc_configs}")



    filter_file = args.filter
    if filter_file is None:
        filter_file = ""
    else:
        if not os.path.isfile(filter_file):
            raise ValueError(f"Filter file does not exists: {filter_file}")
        filter_file = os.path.abspath(filter_file)

    exp_name = args.name

    # Loading the module
    case_mod = __import__(args.module)


    foam_env = FoamEnv("/work/projects/project01921/OpenFOAM/v2212/OpenFOAM")
    foam_env.add_variant("gcc", "etc-gcc/prefs.sh", ["gcc/11.2", "openmpi/4.1.4"])
    foam_env.add_variant("scorep", "etc-scorep/prefs.sh", ["gcc/10.2", "openmpi/4.1.4", "papi", "scorep/7.1"])


    cpu_bind = args.cpu_bind

    mesh_dir = args.mesh_dir

    exp = Experiment(case_mod, foam_env, cpu_bind, filter_file, papi_ctrs, exp_name, case_dir, mesh_dir, base_target_dir)

    for nodes,proc_list in proc_configs.items():
        for np in proc_list:
            exp.setup_scaling_config(nodes, np)

class FoamEnv:
    # variants is a dict that maps the prefs file and the required modules to each variant name
    def __init__(self, foam_dir):
        self.foam_dir = foam_dir
        self.variants = dict()

    def add_variant(self, variant_name, prefs_file, module_list):
        self.variants[variant_name] = (prefs_file, module_list)

    def get_modules(self, variant_name):
        return self.variants[variant_name][1]

    def get_prefs_file(self, variant_name):
        return self.variants[variant_name][0]

    def get_prefs_file_full(self, variant_name):
        return os.path.join(self.foam_dir, self.get_prefs_file(variant_name))



class Experiment:
    def __init__(self, case_mod, foam_env, cpu_bind, filter_file, papi_ctrs, name, case_dir, mesh_dir, base_target_dir):
        self.case_mod = case_mod
        self.foam_env = foam_env
        self.cpu_bind = cpu_bind
        self.filter_file = filter_file
        self.papi_ctrs = papi_ctrs
        self.name = name
        self.case_dir = case_dir
        self.mesh_dir = mesh_dir
        self.target_dir = os.path.join(base_target_dir, name)
        print(f"Creating experiment in {self.target_dir}")
        if not os.path.exists(base_target_dir):
            raise RuntimeError("Experiment base directory does not exists")
        if not os.path.exists(case_dir):
            raise RuntimeError("Case directory does not exists")
        if os.path.exists(self.target_dir):
            raise RuntimeError("Experiment directory already exists")
        os.mkdir(self.target_dir)


    def setup_scaling_config(self, num_nodes, num_procs):
        config_dir = os.path.join(self.target_dir, str(num_procs))
        if os.path.exists(config_dir):
            raise RuntimeError("Scaling config directory already exists")
        #os.mkdir(config_dir)

        self.case_mod.copy_setup(self.case_dir, config_dir, self.mesh_dir)

        self.case_mod.configure(config_dir, num_nodes, num_procs)

        preproc_file = self.case_mod.create_preproc_script(self.foam_env, config_dir, num_nodes, num_procs, self.cpu_bind)
        solve_file = self.case_mod.create_solve_script(self.foam_env, self.filter_file, self.papi_ctrs, config_dir, num_nodes, num_procs, self.cpu_bind)

        self.generate_submit_script(config_dir, preproc_file, solve_file)

    def generate_submit_script(self, cfg_dir, preproc_file, solve_file):
        submit_file = os.path.join(cfg_dir, "submit_jobs.sh")
        with open(submit_file, 'w') as f:
            f.write("#!/bin/bash\n")
            f.write(f"if [ ! -f log.blockMesh ] || [ ! -f log.decomposePar ]; then")
            f.write(f"  sbatch {preproc_file} > sbatch_out.tmp\n")
            f.write("  jobid=$(cat sbatch_out.tmp | head -n 1 | cut -d \" \" -f 4)\n")
            f.write("  dep=\"--depend=afterok:${jobid}\"\n")
            f.write(f"  sbatch $dep {solve_file}\n")
            f.write("  rm sbatch_out.tmp\n")
            f.write("else\n")
            f.write("  echo \"Preprocessing already done. Remove logs to rerun.\"")
            f.write("  sbatch pitz_daily_solve_2.job\n")
            f.write("fi\n")
        st = os.stat(submit_file)
        os.chmod(submit_file, st.st_mode | stat.S_IEXEC)


if __name__ == '__main__':
   main()

