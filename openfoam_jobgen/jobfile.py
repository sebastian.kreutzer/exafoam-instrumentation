import io
import os
from string import Template

class JobFileWriter:
    def __init__(self, job_name, foam_dir):
        self.job_name = job_name
        self.foam_dir = foam_dir
        self.writer = io.StringIO()

    def output_to_file(self, filename):
        with open(filename, 'w') as fout:
            fout.write(self.writer.getvalue())

    def write_line(self, line=""):
        self.writer.write(line + "\n")

    def sbatch_param(self, name, val):
        self.write_line(f"#SBATCH {name} {val}")

    def sbatch_param_str(self, str):
        self.write_line(f"#SBATCH {str}")

    def write_header(self, case_dir, num_nodes, num_procs, mem_per_cpu, time_limit, exclusive):
        self.write_line("#!/bin/bash")
        self.write_line()
        self.sbatch_param("-e", os.path.join(case_dir, f"{self.job_name}_%j.err"))
        self.sbatch_param("-o", os.path.join(case_dir, f"{self.job_name}_%j.out"))
        self.write_line();
        self.sbatch_param("-J", self.job_name)
        self.sbatch_param("-N", num_nodes)
        self.sbatch_param("-n", num_procs)
        if exclusive:
            self.sbatch_param_str("--exclusive")
        self.sbatch_param_str(f"--mem-per-cpu={mem_per_cpu}")
        self.sbatch_param("-t", time_limit)
        # TODO: Configurable project setup
        self.sbatch_param("-C", "avx512")
        self.sbatch_param("-A", "project01921")
        self.write_line()

    def load_modules(self, module_list):
        self.write_line("module purge")
        for m in module_list:
            self.write_line(f"module load {m}")
        self.write_line()


    def load_foam_cfg(self, prefs_file):
        bashrc_file = os.path.join(self.foam_dir, "etc/bashrc")
        self.write_line(f"source {bashrc_file} {prefs_file}")
        self.write_line()

    def load_run_functions(self):
        run_funcs = os.path.join(self.foam_dir, "bin/tools/RunFunctions")
        self.write_line(f". {run_funcs}")
        self.write_line()

    def export(self, name, val):
        self.write_line(f"export {name}={val}")

    def disable_scorep(self):
        self.export("SCOREP_EXPERIMENT_DIRECTORY", "")
        self.export("SCOREP_ENABLE_PROFILING", "false")
        self.export("SCOREP_ENABLE_TRACING", "false")


    def configure_scorep_profiling(self, filter_file, papi, output_dir):
        self.export("SCOREP_EXPERIMENT_DIRECTORY", output_dir)
        self.export("SCOREP_ENABLE_PROFILING", "true")
        self.export("SCOREP_ENABLE_TRACING", "false")
        self.export("SCOREP_TOTAL_MEMORY", "400M")
        self.export("SCOREP_METRIC_PAPI_PER_PROCESS", papi)
        if filter_file is not None:
            self.export("SCOREP_FILTERING_FILE", filter_file)

    def timed_srun(self, cmd, name, cpu_bind="cores"):
        self.write_line(f"srun --cpu-freq=HighM1-HighM1 --cpu-bind=verbose,{cpu_bind} --distribution=block:cyclic /usr/bin/time --format=\"Elapsed time ({name}): %E (%e s)\nMemory: %M kBytes\" -- {cmd}")


    def insert_template(self, template_file, substitutions={}):
        """Copies the contents of the given template into the job file.
        Variables marked with $ are substituted according to the given dictionary."""
        input = open(template_file, 'r').read()
        t = Template(input)
        res = t.substitute(**substitutions)
        self.write_line(res)


class CommandSequence:

    def  __init__(self):
        self.cmds = []

    def add_command(self, cmd, profiled):
        self.append((cmd, profiled))

class ExecutionEnv:
    def  __init__(self, foam_variant):
        self.foam_variant = foam_variant
        self.profiling = False
        self.scorep_out_dir = ""
        self.filter_file = ""

    def enable_profiling(scorep_out_dir, filter_file):
        self.profiling = True
        self.scorep_out_dir = scorep_out_dir
        self.filter_file = filter_file

class Command:
    def __init__(self, cmd):
        self.cmd = cmd

    def write(self, job):
        job.write_line(cmd)

class TimedSrunCommand(Command):
    def __init__(self, cmd, name):
       super().__init__(self, cmd)
       self.name = name

    def write(self, job):
       job.timed_srun(cmd, name)

