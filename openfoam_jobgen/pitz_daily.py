import shutil
import os

from jobfile import JobFileWriter
import foam_util

def copy_setup(case_dir, target, mesh_dir=None):
    shutil.copytree(case_dir, target)
    if mesh_dir:
        shutil.copytree(os.path.join(mesh_dir, "constant/polyMesh"), os.path.join(target, "constant/polyMesh"))

def configure(working_dir, num_nodes, num_procs):
    foam_util.set_dict_entry(os.path.join(working_dir, "system/decomposeParDict"), "numberOfSubdomains", num_procs)
    if num_procs / num_nodes % 2 == 0:
        foam_util.set_dict_entry(os.path.join(working_dir, "system/decomposeParDict"), "multiLevelCoeffs.domains", f"({num_nodes} 2 {num_procs / num_nodes / 2})")
    else:
        if num_procs == 1:
            foam_util.set_dict_entry(os.path.join(working_dir, "system/decomposeParDict"), "multiLevelCoeffs.domains", f"(1 1 1)")
        else:
            raise ValueError(f"num_procs / num_nodes must be even")



def create_preproc_script(foam_env, working_dir, num_nodes, num_procs, cpu_bind):
    job_name = f"pitz_daily_preproc_{num_procs}"
    preproc_job = JobFileWriter(job_name, foam_env.foam_dir)
    preproc_job.write_header(working_dir, 1, 1, "32000M", 10, False)
    preproc_job.load_modules(foam_env.get_modules("gcc"))
    preproc_job.load_foam_cfg(foam_env.get_prefs_file_full("gcc"))
    preproc_job.load_run_functions()

    preproc_job.insert_template("pitz_daily_parts/pre_blockMesh.job.part")
    preproc_job.write_line("runApplication blockMesh")
    #preproc_job.timed_srun("runApplication blockMesh", "blockMesh")

    preproc_job.insert_template("pitz_daily_parts/determine_restart.job.part")
    preproc_job.write_line("runApplication decomposePar")
    #preproc_job.timed_srun("runApplication decomposePar", "decomposePar")

    preproc_job.insert_template("pitz_daily_parts/create_foam.job.part")
    #preproc_job.timed_srun("renumberMesh -parallel -overwrite", "renumberMesh")

    job_filename = f"{job_name}.job"
    preproc_job.output_to_file(os.path.join(working_dir, job_filename))
    return job_filename

def create_solve_script(foam_env, filter_file, papi_ctrs, working_dir, num_nodes, num_procs, cpu_bind):
    job_name = f"pitz_daily_solve_{num_procs}"
    solve_job = JobFileWriter(job_name, foam_env.foam_dir)
    solve_job.write_header(working_dir, num_nodes, num_procs, "2000M", 10, True)

    solve_job.write_line()
    solve_job.write_line("""trap "exit -1" ERR""")
    solve_job.write_line()

    # Renumber mesh
    solve_job.load_modules(foam_env.get_modules("gcc"))
    solve_job.load_foam_cfg(foam_env.get_prefs_file_full("gcc"))
    solve_job.timed_srun("renumberMesh -overwrite -parallel", "renumberMesh", cpu_bind)

    solve_job.write_line()

    # Solve
    solve_job.load_modules(foam_env.get_modules("scorep"))
    solve_job.load_foam_cfg(foam_env.get_prefs_file_full("scorep"))
    solve_job.load_run_functions()
    solve_job.configure_scorep_profiling(filter_file, papi_ctrs, f"scorep-pitz-daily-{num_procs}")


    solve_job.timed_srun("XiFoam -parallel", "XiFoam", cpu_bind)

    job_filename = f"{job_name}.job"
    solve_job.output_to_file(os.path.join(working_dir, job_filename))
    return job_filename
