export FOAM_CONFIG_ETC="etc-scorep"

export SCOREP_WRAPPER_COMPILER_FLAGS=
export SCOREP_WRAPPER_INSTRUMENTER_FLAGS="--nomemory --verbose"
export SCOREP_PROFILING_MAX_CALLPATH_DEPTH=64

# Score-P can either support generating serial code or parallel/mpi code (default=parallel) - but never both at the same time.
# If you run an application instrumented by Score-P with parallel support in serial,
# Score-P will not be able to dump its results to disk but will report an error at application end.
# For an OpenFOAM installation for serial support uncomment the following.
#export SCOREP_WRAPPER_INSTRUMENTER_FLAGS="--mpp=none ${SCOREP_WRAPPER_INSTRUMENTER_FLAGS}"

# configure environment
export WM_COMPILER=GccScoreP
