#!/bin/bash

experiment_dir=$1


if [ ! -d "$experiment_dir" ]; then
  echo "Please pass the experiment directory"
  exit
fi


exp_path=$(cd $experiment_dir && pwd)
exp_name=$(basename $exp_path)


out_dir="extrap_${exp_name}"

if [ -d "$out_dir" ]; then
  echo "Extra-P directory already exists!"
  exit
fi


for dir in $experiment_dir/*/; do
  echo "$dir"
  dir_name="${exp_name}.np$(basename $dir)..r1"
  echo "Saving in $dir_name"
  find $dir -name "*.cubex" -exec mkdir -p $out_dir/$dir_name \; -exec cp {} $out_dir/$dir_name \;
done


