#!/bin/bash

export PROJECT_DIR=/work/projects/project01921

export CGC_DIR=/work/groups/da_sc/exafoam/packages/.gcc/10.2/metacg/devel/bin/
export CLANG_DIR=/shared/apps/.gcc/10.2/llvm/10.0.0/lib/clang/10.0.0/


function foam_find_symbol {
    libdir=$(dirname $(which simpleFoam))/..
    find "$libdir" -type f -exec sh -c "echo {}; nm {} | grep $1" \; | less
}

