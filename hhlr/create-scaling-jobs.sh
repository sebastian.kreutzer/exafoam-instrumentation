#!/bin/bash

script_dir=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
base_dir="$script_dir/.."
project_dir="/work/projects/project01667"

if [ -z "$1" ]; then
  echo "Please pass a name for the new experiemnt"
  exit
fi

experiment_name="$1"


foam_dir="$WM_PROJECT_DIR"

if [ ! -d "$foam_dir" ]; then
  echo "WM_PROJECT_DIR not set. Please load the desired OpenFOAM configuration."
  exit
fi

echo "Using OpenFOAM installation in $foam_dir"

experiment_dir="$project_dir/$experiment_name"

if [ -d "$experiment_dir" ]; then
    echo "Experiment $experiment_dir already exists! Please choose a different name."
    exit
fi

mkdir $experiment_dir

echo "Working from $base_dir"
echo "Creating jobs in $experiment_dir"


# Selecting Test-case
echo -n "Select the desired Test-case (Enter 1 for Lid-driven cavity, 2 for Motorbike, 3 for conical diffuser RAS): "
read testcase_num
case $testcase_num in
   1)
      echo "Lid-driven cavity selected"
      testcase="Lid_driven_cavity-3d"
      solver="icoFoam"
      testcase_short="ldc"
      filter_file="ldc.filt"
      ;;
   2)
      echo "Motorbike selected"
      testcase="HPC_motorbike"
      solver="simpleFoam"
      testcase_short="motorbike"
      filter_file="motorbike.filt"
      ;;
   3)
      echo "Conical Diffuser RAS selected"
      testcase="conicalDiffuserRAS"
      solver="simpleFoam"
      testcase_short="codiffras"
      filter_file="codiffras.filt"
      ;;
esac

# Selecting Dimension
echo "Select the desired dimensions among the following: "
ls $base_dir/${testcase}/
read dimension

# Selecting Convergence Criteria
echo "Select the desired fvSolution file among the following: "
ls $base_dir/${testcase}/${dimension}/system/ | grep fvSolution
read conv_file

conv_crit="${conv_file:11}"

if [ "$testcase_short"  == "ldc" ] || [ "$testcase_short" == "motorbike" ]; then

  echo "Choose a solver (1 FOAM-DIC-PCG, 2 PETSc-ICC-CG): "
  read solver_config
  case $solver_config in
    1)
      echo "Selected FOAM-DIC-PCG"
      use_petsc=0
      p_solver="PCG"
      p_precond="DIC"
      solver_short="dic_pcg"
      ;;
    2)
      echo "Selected PETSc-ICC-PCG"
      use_petsc=1
      p_solver="petsc"
      p_precond="petsc"
      solver_short="petsc_icc_pcg"
      ;;
  esac
else
  echo "Using default solver"
  use_petsc=0
  p_solver=""
  p_precond=""
  solver_short=""
fi


node_distr=block

# Always use profiling for scaling analysis
scorep_mode="profiling"
enable_profiling="true"
enable_tracing="false"


echo "Select PAPI counters (Enter 1 for no counters, 2 for 'PAPI_TOT_INS,PAPI_TOT_CYC', 3 for custom counters)"
read papi_ctrs
case $papi_ctrs in
  1)
    echo "No PAPI counters"
    papi_str=""
    ;;
  2)
    papi_str="PAPI_TOT_INS,PAPI_TOT_CYC"
    echo "$papi_str"
    ;;
  3)
    echo "Enter desired PAPI counters:"
    read papi_str
    echo "$papi_str"
    ;;
esac

echo "Choose the number of processes to evaluate (e.g. '1 2 4 8')"
read np_list

# Note: we use (1 1 $np) dimensions, so any value for $np is fine
# for np in $np_list; do
#   if [ "$testcase_num" -eq 3 ]; then
#     if (($np % 4 != 0)); then
#       echo "Number of processes must be a mulitple of 4!"
#       exit
#     fi
#   fi
# done

echo "Set endTime (default for S: 0.1)"
read end_time

# Only one node
nodes="1"


cd $experiment_dir

cat > "config.txt"  << EOL
testcase=${testcase_short}
problem_size=$dimension
solver=$solver_short
papi_counters=$papi_str
num_nodes=1
num_processes=$np_list
scorep_mode=profiling

EOL

if [ ! -f "$base_dir/$testcase/$filter_file" ]; then
  echo "Filter file $filter_file does not exist!"
  echo "Path: $base_dir/$testcase/$filter_file"
  exit
fi


for np in $np_list; do

    cd $experiment_dir

    case_dir="${np}"

    if [ -d "$case_dir" ]; then
      echo "Test case $case_dir already exists"
      exit
    fi

    mkdir $case_dir

    # Copy testcase

    cp -r $base_dir/$testcase/$dimension/* $case_dir

    cp $base_dir/$testcase/$filter_file $case_dir

    echo "Working in $case_dir"

    cd $case_dir

    full_case_dir=$(pwd)

#    module load gcc/10.2;
#    module load openmpi/4.1;
#    module load boost/1.75.0;
#    module load cmake;
#    module load fftw/3.3.9;
#    module load cgal/5.2.0;
#    module load scotch;
#    module load papi;
#    module load scorep/7.1;

    ./Allclean
    rm -rf scorep*

    . ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions

    foamDictionary system/decomposeParDict -entry numberOfSubdomains -set $np

    if [ "$testcase_num" -eq 3 ]; then
        n_coeff=$np
        foamDictionary system/decomposeParDict -entry simpleCoeffs.n -set "(1 1 $n_coeff)"
    fi

    echo $conv_crit
    cp system/$conv_file system/fvSolution

    if [ -n "$p_solver" ]; then
      foamDictionary system/fvSolution -entry solvers.p.solver -set $p_solver
      foamDictionary system/fvSolution -entry solvers.p.preconditioner -set $p_precond
    fi

    foamDictionary system/controlDict -entry endTime -set $end_time

    petsc_load_command=""
    if [ $use_petsc -eq 1 ]; then
      petsc_load_command="module load petsc/3.14
    foamEtcFile -sh -config petsc -- -force"
      foamDictionary system/controlDict -entry libs -set "(petscFoam)"

      # Note: System PETSc does not support mpiaijmkl
      petsc_solver_cfg="{options{ksp_type cg;pc_type bjacobi;sub_pc_type icc;ksp_cg_single_reduction true;mat_type mpiaij;sub_pc_factor_shift_type none;}caching{matrix{update always;}preconditioner{update always;}}}"

      foamDictionary system/fvSolution -entry solvers.p.petsc -set "$petsc_solver_cfg"
      foamDictionary system/fvSolution -entry solvers.pFinal -set '{$p;relTol 0;}'
    fi


    srun_args="--cpu-freq=HighM1-HighM1 --cpu-bind=verbose,cores --distribution=$node_distr,*,*"


    case $testcase_num in
       1) # Cavity
          mesh_command="runApplication blockMesh"
          init_command=""
          time_limit="120"
          ;;
       2) # Motorbike
          mesh_command="./Allmesh${dimension}
          ./Allclean
          cp -r 0.org 0"
          init_command="srun $srun_args /usr/bin/time --format=\"Elapsed time: %E (%e s)\nMemory: %M kBytes\" -- potentialFoam -writephi -parallel"
          time_limit="120"
          filter_file="motorbike.filt"
          ;;
       3) # Conical diffuser
          mesh_command="pushd system/ >& /dev/null
          m4 -P blockMeshDict.m4 > blockMeshDict
          popd >& /dev/null
          runApplication blockMesh
          cp -r 0_orig 0
          runApplication addSwirlAndRotation"
          init_command="srun $srun_args /usr/bin/time --format=\"Elapsed time (potentialFoam): %E (%e s)\nMemory: %M kBytes\" -- potentialFoam -parallel
          srun $srun_args /usr/bin/time --format=\"Elapsed time (renumberMesh): %E (%e s)\nMemory: %M kBytes\" -- renumberMesh -parallel -overwrite"
          time_limit="120"
          filter_file="codiffras.filt"
          ;;
    esac

    run_command="srun $srun_args /usr/bin/time --format=\"Elapsed time: %E (%e s)\nMemory: %M kBytes\" -- $solver -parallel"


    preproc_file="preproc.job"
    cat > ${preproc_file} << EOL
#!/bin/bash

#SBATCH -J preproc_${case_dir}

#SBATCH -e ${full_case_dir}/preproc_%j.err
#SBATCH -o ${full_case_dir}/preproc_%j.out


#SBATCH -N 1
#SBATCH -n 1

#SBATCH --mem-per-cpu=8000
#SBATCH -t 30

#SBATCH -C "avx512"
#SBATCH -A project01667


module purge
module load gcc/10.2;
module load openmpi/4.1;
module load boost/1.75.0;
module load cmake;
module load fftw/3.3.9;
module load cgal/5.2.0;
module load scotch;
module load papi;
module load scorep/7.1;

source ${foam_dir}/etc/bashrc ${foam_dir}/etc-scorep/prefs.sh

${petsc_load_command}

. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions

$mesh_command
runApplication decomposePar
EOL

    solve_file="solve.job"
    cat > ${solve_file} << EOL
#!/bin/bash

#SBATCH -J solve_${case_dir}

#SBATCH -e ${full_case_dir}/solve_%j.err
#SBATCH -o ${full_case_dir}/solve_%j.out


#SBATCH -N $nodes
#SBATCH -n $np

#SBATCH --mem-per-cpu=4000
#SBATCH -t $time_limit

#SBATCH -C "avx512"
#SBATCH -A project01667

module purge
module load gcc/10.2;
module load openmpi/4.1;
module load boost/1.75.0;
module load cmake;
module load fftw/3.3.9;
module load cgal/5.2.0;
module load scotch;
module load papi;
module load scorep/7.1;

source ${foam_dir}/etc/bashrc ${foam_dir}/etc-scorep/prefs.sh

${petsc_load_command}


. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions

export SCOREP_EXPERIMENT_DIRECTORY=
export SCOREP_ENABLE_PROFILING=false
export SCOREP_ENABLE_TRACING=false
$init_command

export SCOREP_FILTERING_FILE=$filter_file
export SCOREP_ENABLE_PROFILING=$enable_profiling
export SCOREP_ENABLE_TRACING=$enable_tracing
export SCOREP_TOTAL_MEMORY=4000M
export SCOREP_METRIC_PAPI=$papi_str
export SCOREP_EXPERIMENT_DIRECTORY=scorep-n${np}-filtered

$run_command

EOL

submit_file="submit_jobs.sh"
cat > ${submit_file} << EOL
#!/bin/bash
sbatch $preproc_file > sbatch_out.tmp
cat sbatch_out.tmp
jobid=\$(cat sbatch_out.tmp | head -n 1 | cut -d " " -f 4)
dep="--depend=afterok:\${jobid}"
sbatch \$dep $solve_file
rm sbatch_out.tmp

EOL
    chmod +x $submit_file

done

echo "Done generating job scripts."



