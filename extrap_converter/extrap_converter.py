import sys
import os
import argparse
import glob
import numpy as np
from pycubexr import CubexParser
from pycubexr.utils.exceptions import MissingMetricError

def main():

    parser = argparse.ArgumentParser(prog='extrap_converter.py')
    parser.add_argument('input_dir', help='Input directory containing experiments')
    parser.add_argument('-o', help='Output file', default='extrap.txt', metavar='outfile')
    parser.add_argument('--inclusive', action='store_true', default=False)
    parser.add_argument('--metric', help='Metric to export', default='time')
    parser.add_argument('--subtract', help='Subtract the measurements for this function so that it is not considered for inclusive time', default=None)
    parser.add_argument('--use_mean', help='Use the mean value across all MPI processes instead of accumulating', default=False, action='store_true')
    args = parser.parse_args()

    input_dir = args.input_dir
    if not os.path.isdir(input_dir):
        raise ValueError(f"Input directory does not exists: {input_dir}")

    folders = [f.path for f in os.scandir(input_dir) if f.is_dir()]

    inclusive = args.inclusive
    print(f"Using inclusive time: {inclusive}")

    # TODO: Support multiple subtractions
    subtract_list = []
    if args.subtract:
       subtract_list.append(args.subtract)

    use_mean = args.use_mean
    if use_mean:
        print(f"Modelling mean values")

    max_profiles_found = 0

    cube_files = {}

    for f in folders:
        fname = os.path.basename(f)
        try:
            num_procs = int(fname)
        except ValueError:
            print(f"Folder is not a valid integer parameter: {fname}")
            continue
        profiles = glob.glob(f"{f}/**/*.cubex", recursive=True)
        print(profiles)
        cube_files[num_procs] = profiles

    data = {}
    regions = []
    metrics = []

    for p, cube_files in cube_files.items():
        if len(cube_files) == 0:
            print(f"Folder {p} does not contain any cube files")
            continue
        data[p] = []
        #print(f"Values for {p}:")
        for cube_file in cube_files:
            data[p].append(read_cube_file(cube_file, [args.metric], inclusive, use_mean, subtract_list))
            if len(metrics) == 0:
                for metric, metric_data in data[p][0].items():
                    metrics.append(metric)
                for region, val in data[p][0]['time'].items():
                    regions.append(region)

    outfile = args.o


    params = data.keys();

    with open(outfile, 'w') as f:
        f.write("PARAMETER p\n")
        point_str = "POINTS"
        for p in sorted(params):
            point_str += f" ({p})"
        f.write(f"{point_str}\n")

        for r in regions:
            f.write(f"REGION {r}\n")
            for m in metrics:
                f.write(f"METRIC {m}\n")
                for p in sorted(params):
                    f.write("DATA")
                    for repeat in data[p]:
                        if r in repeat[m]:
                            f.write(f" {repeat[m][r]}")
                        else:
                            f.write(" 0")
                    f.write("\n")


            f.write('\n')




def read_cube_file(cube_file, metrics, inclusive, use_mean, subtract_list):
    profile_data = {}
    with CubexParser(cube_file) as cubex:
        # iterate over all metrics in cubex file
        for metric in cubex.get_metrics():
            # return the name of the current metric
            metric_name = metric.name

            if metric_name not in metrics:
                continue

            #print(metric_name)
            try:
                metric_values = cubex.get_metric_values(metric=metric)

                profile_data[metric_name] = {}

                visited = []

                def add_cnode_data(parent_chain, cnode):
                    visited.append(cnode)
                    region = cubex.get_region(cnode)

                    # return the name of the current region
                    region_name = region.name
                    # print(region_name)
                    # return the measurement values for all mpi processes for the current metric and callpath
                    #cnode_values = metric_values.cnode_values(cnode, convert_to_inclusive=inclusive,
                    #                                          convert_to_exclusive=not inclusive)
                    if use_mean:
                        cnode_values = metric_values.mean(cnode, convert_to_inclusive=inclusive,
                                                       convert_to_exclusive=not inclusive)
                    else:
                        cnode_values = metric_values.value(cnode, convert_to_inclusive=inclusive,
                                                               convert_to_exclusive=not inclusive)

                    if subtract_list and region_name in subtract_list:
                        # Subtract value from all caller nodes
                        for p in parent_chain:
                            profile_data[metric_name][p] -= cnode_values
                        return

                    if not parent_chain:
                        path_str = region_name
                    else:
                        path_str = f"{parent_chain[-1]}->{region_name}"
                    parent_chain.append(path_str)

                    # if path_str == '':
                    #     new_path_str = region_name
                    # else:
                    #     new_path_str = f"{path_str}->{region_name}"
                    profile_data[metric_name][path_str] = cnode_values
                    for child in cnode.get_children():
                        #print(f"Child: {child.region.name}")
                        if child not in visited:
                            add_cnode_data(list(parent_chain), child)

                for root in cubex.get_root_cnodes():
                    add_cnode_data([], root)

                # iterate over all callpaths in cubex file
                # for callpath_id in range(len(metric_values.cnode_indices)):
                #     cnode = cubex.get_cnode(metric_values.cnode_indices[callpath_id])
                #
                #     # return the current region i.e. callpath
                #     region = cubex.get_region(cnode)
                #
                #
                #     # return the name of the current region
                #     region_name = region.name
                #     print(region_name)
                #     # return the measurement values for all mpi processes for the current metric and callpath
                #     cnode_values = metric_values.cnode_values(cnode, convert_to_inclusive=inclusive, convert_to_exclusive=not inclusive)
                #     print(cnode_values)
            except MissingMetricError as e:
                print(f"Missing metric '{metric_name}'")

    return profile_data


if __name__ == '__main__':
    main()

