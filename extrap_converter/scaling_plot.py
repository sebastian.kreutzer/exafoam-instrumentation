import argparse
import sys
import numpy as np
import matplotlib.pyplot as plt
import scienceplots
import re

from numpy import log2
import pandas as pd

def eval_model(model_str, param_name, val):
  #print(f"Evaluating model: {model_str}")
  exec("%s = %f" % (param_name, val))
  return eval(model_str)


def parse_function_string(input_string):
    # Replace '^' with 'np.power'
    input_string = re.sub(r'(\w+)\^(\(?\d+(?:\.\d+)?\/\d+(?:\.\d+)?\)?)', r'np.power(\1, \2)', input_string)
    print(input_string)
    # Replace 'log2' with 'np.log2'
    input_string = re.sub(r'log2\((\w+)\)', r'np.log2(\1)', input_string)
    # Replace 'log' with 'np.log'
    input_string = re.sub(r'log\((\w+)\)', r'np.log(\1)', input_string)
    # Replace 'exp' with 'np.exp'
    input_string = re.sub(r'exp\((\w+)\)', r'np.exp(\1)', input_string)
    input_string = re.sub(r'\^', r'**', input_string)
    return input_string

def extract_data_partial(lines):
    data = {}
    i = 0
    while i < len(lines):
        line = lines[i].strip()
        points = []
        mean_data = []
        parsed_model = []
        fname = line
        print(f"function: {fname}")
        i += 1
        while i < len(lines):
            line = lines[i].strip()
            if not line:
                break;
            elif line.startswith("Model:"):
                model_str = line[7:]
                parsed_model = parse_function_string(model_str)
                print(f"Parsed model: {parsed_model}")
            else:
                print(f"data: {line}")
                point, mean = eval(line)
                points.append(point)
                mean_data.append(mean)
            i += 1
        data[fname] = (points, mean_data, parsed_model)
        i += 1
    return data


def extract_data(lines):
    data = {}
    i = 0
    while i < len(lines):
        line = lines[i].strip()
        print(line)
        if line.startswith('Callpath:'):
            print(line.split(':', 1))
            callpath = line.split(':', 1)[1].strip()
            mean_data = []
            points = []
            parsed_model = ""
            i += 1
            while i < len(lines):
                line = lines[i].strip()
                if lines[i].startswith('Callpath:'):
                    break
                elif line.startswith('Metric:'):
                    metric = line.split(':')[1].strip()
                elif line.startswith('Measurement point:'):
                    parts = line.split()
                    point = float(parts[2][1:-1])
                    points.append(point)
                    mean = float(parts[4])
                    mean_data.append(mean)
                elif line.startswith('Model:'):
                    model = line.split(":")[1].strip()
                    parsed_model = parse_function_string(model)
                    print(f"Model: {model}")
                    print(f"Translated Model: {parsed_model}")
                i += 1
            data[callpath] = (points, mean_data, parsed_model)
    return data

def main():
    parser = argparse.ArgumentParser(prog='scaling_plot.py')
    parser.add_argument('input_file', help='Input file')
    parser.add_argument('--max', help='Max parameter value to plot', required=True, type=int)
    parser.add_argument('--param', help='Parameter name', required=True)
    parser.add_argument('--filter', help='Filter file containing include list of functions to plot (separated by newlines)')
    parser.add_argument('--title', help='Plot title', default='Performance Model')
    parser.add_argument('--style', help='Plot style name', default='fivethirtyeight')

    args = parser.parse_args()

    param_name = args.param
    infile = args.input_file
    maxval = int(args.max)
    title = args.title

    fn_list = []
    if args.filter:
        with open(args.filter) as f:
            fn_list = [l.strip() for l in f.readlines()]

    with open(infile) as f:
        lines = f.readlines()

    lines = list(filter(None, lines))

    if "Callpath:" in lines[0].strip():
        # Full export
        data = extract_data(lines)
    else:
        data = extract_data_partial(lines)

    print(plt.style.available)

    print(data)

    plt.style.use(args.style)
    plt.gca().xaxis.grid(False)

    plt.title(title)
    plt.xlabel(param_name)
    plt.ylabel("Time [s]")

    legend_str = []

    model_plots = []

    print(fn_list)

    for callpath, fdata in data.items():

        print("Handling ")

        fname = callpath.split("->")[-1]

        if fn_list and fname not in fn_list:
            print(f"Ignoring {fname}")
            continue

        param_vals = fdata[0]
        measured_vals = fdata[1]
        model_str = fdata[2]

        legend_str.append(fname)

        model_x = np.arange(1, maxval, 1)
        model_y = [eval_model(model_str, param_name, x) for x in model_x]

        m = plt.plot(model_x, model_y, label=fname)
        model_plots.append(m)

        plt.plot(param_vals, measured_vals, color=m[0].get_color(), marker='x', linestyle='')

        print(f"{fname} model: {model_str}")
        print(param_vals)
        print(measured_vals)

    # plt.legend(model_plots,legend_str)
    plt.legend(framealpha=1, frameon=True)

    plt.show()

    return


if __name__ == "__main__":
  main()

